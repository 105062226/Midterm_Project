# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer
## Topic
* [NTHU Chat]
* Key functions (add/delete)
    1. [Chat]
    2. [Load message history]
    3. [Chat with new user]
* Other functions (add/delete)
    1. [Let user choose and change personal Selfie]
    2. [Show time]
    3. [On the left up corner of chat room show personal email and personal selfie]
    4. [Message send by user will on the right,by others will on the left]
    5. [Detect who hasn't sign-out]

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|GitLab Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|
|Other functions|1~10%|Y|

## Website Detail Description
1.Membership Mechanism(20%)
    -打上電郵密碼,在sign-up page按下new account即可以自創自己的帳戶
    -若已創建自己的帳戶,同樣在sign-up page輸入自己創建的帳號密碼即可登入

2.Host on your GitLab(Firebase) Page(5%)
    -https://ss-midterm-project.firebaseapp.com

3.Database read/write(15%)
    -javsscript檔裡內含許多firebase data read/write的功能,用來在聊天室向使用者顯示正確的內容,後面report會有更詳細的說明

4.RWD(15%)
    -參考網路上的RWD template做修改,並在行動裝置等各式moblie device 做測試確實能有RWD的效果

5.Topic Key Functions(15%)
    -我所做的即是第一項Chat room(chat,load message history,chat with new user)

6.Sign Up/In with Google or other third-party accounts(2.5%)
    -在sign-in page 時有提供google facebook 登入方法

7.ADD Chrome notification(5%)
    -在使用者進入Chat room 畫面時,會跳出是否允許網頁顯示通知,若允許,即會顯示"Welcome to NTHU Chat"的通知
    
8.Use CSS animation(2.5%)
    -在singn-up page 圖片背景顏色會隨時間做變動

9.Proove your website has strong security(5%)
    -後面report會有更詳細的說明

10.Other functions not included in key functions(1~10%)
    -讓使用者可選擇並且可在後續更換在聊天室裡的個人大頭貼
    -可以顯示發送時間
    -在Chat room左上角顯示個人電郵及現在使用之大頭貼
    -使用者使用時,自己發的訊息會靠右,別人發的訊息會靠左
    -偵測誰還沒登出
  
Midterm Project Report

1. Sign in/Sign up 實作:
    利用firebase內建的"signInWithEmailAndPassword"及"createUserWithEmailAndPassword"在加上對sign-up button及sign-in button
    用addEventListener來聽取他們被'click'的動作之後就啟動前面提到的兩個function即可完成基本的sign-up/sign-in的功能

2. Sign Up/In with Google or other third-party accounts實作:
    照著firebase講義上提供如何使用第三方登入的方式再加上同樣對google facebook botton增加addEventListener聽取'click'事件即可即可完成第三方登入的功能

3. Sign-out實作
    根據Lab06-SimpleForum作法把對應的sign-out按鈕加上即可addEventListener聽取'click'事件即可完成

4. Chat room 實作:
    在這我將caht room的實作分為三個部分來說明分別為:顯示訊息及發送時間,顯示正確的大頭貼及分清楚左右訊息框,顯示誰還沒登出
    
(a)顯示訊息及發送時間

(1)先創一個com_list的資料型態,每當我按下submit鍵時,裡面會被push上去四個資訊分別是使用者輸入的message及他的sending time,而sending-time是用javascript裡內建的"Date()"裡"toLocaleTimeString()":

![](https://i.imgur.com/dk6vqqX.png)

來取得的還有user裡面放的是發送者的email最後是他的
user-image如附圖:

![](https://i.imgur.com/tFdD1BD.png)


,即可得到以下的firrbase data_structure:

![](https://i.imgur.com/NAnICLv.png)


這樣即可將發訊息的歷史紀錄都推送至firebase上

(2)取得com_list這個database

![](https://i.imgur.com/jDENeI5.png)

接著使用firebase snapshot及childsnapshot取得com_list裡的child
將已被推送上database的歷史紀錄給加入至顯示的message-box的innerHTML裡

![](https://i.imgur.com/jStO6Pf.png)

即可顯示歷史紀錄

(3)接著還要偵測當發送新訊息必須將最後加進去com_list裡的資訊也一併顯示在message-box裡,所以必須偵測"child_added"事件,當child_added時
我就利用firebase裡一個名叫"orderByKey().limitToLast(1)"的function他會幫我找到最後一個被加進去的node我再利用snapshot將裡面的使用者資訊給拿出來,也用相同的方式加入我想顯示的innerHTML裡,最後再利用jQuery裡內建的使聊天欄裡scroll bar會緩慢下降的function來使得當使用者
輸入訊息之後scroll bar會緩慢下降

![](https://i.imgur.com/9GcB2df.png)


(b)顯示正確的大頭貼及分清楚左右訊息框
            
(1)我實作大頭貼的方式是將當使用者登入之後,會進入一個大頭貼選擇頁面,在使用者選擇完大頭貼後,並點下進入聊天室後,我就會建立一個num_of_user的
database,在num_of_user根據取得"firebase.auth().currentUser"裡的uid在下面set picture,who_is_online兩個data 

![](https://i.imgur.com/19g5vW3.png)

而picture會根據他點按的大頭貼將他對應圖片的字串給push上去

(2)接著同樣使用firebase裡的snapshot取得picture的字串,將其對應的圖片連結給set到com_list裡訊息的user_image裡,接著只要將com_list裡的user_image
顯示抓出來顯示即可完成大頭貼的實作如附圖

![](https://i.imgur.com/endh14C.png)

![](https://i.imgur.com/0xYk1IG.png)

![](https://i.imgur.com/gpHEWAL.png)


(3)分辨左右訊息框,我採取的作法是將user-email跟com_list裡的user存的email比較,若相同及代表該歷史紀錄是由使用者本人所發出,其他則相反,接著將其增加的
inner_HTML裡的span分別加上不同的id一個為user_send一個為other_send如附圖

![](https://i.imgur.com/dPWLOMl.png)

,接著在css檔裡對user_send
這個id做靠右的css-style即可完成如附圖:

![](https://i.imgur.com/KW5NbUI.png)

,在child_added也就是發送訊息時也是如此

(c)顯示誰還沒sign-out
(1)由前面可以知道,當我在選單裡選擇大頭貼後進入後,會set "num_of_user"這個database,並且他底下是根據user的uid來set的,故我只要在當有人按下sigh-out鍵
之後將num_of_user裡面的user.uid的node給remove掉,剩下的就會是還沒sign out的user的資料:

![](https://i.imgur.com/3i6W5un.png)

(2)接著我只要偵測,利用firebase data snapshot走過該database裡所有的node將裡面的who_is_online全部顯示在我的HTML上即可:

![](https://i.imgur.com/B8BISQq.png)


## Security Report (Optional)

可以偵測基本的sign-up的錯誤,例如密碼小於6個字元,帳號不是email形式,來使我的用戶能做基本的過濾,再來設置rule讓沒登入的人不能寫入或讀取我的database裡的資料,
以確保不會被使用者以外的人作惡意的更改,使我的網頁非常之安全!

![](https://i.imgur.com/moT2q6V.png)


