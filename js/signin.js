function initApp() {
    // Login with Email/Password
    var txtEmail = document.getElementById('inputEmail');
    var txtPassword = document.getElementById('inputPassword');
    var btnLogin = document.getElementById('btnLogin');
    var btnGoogle = document.getElementById('btngoogle');
    var btnSignUp = document.getElementById('btnSignUp');
    var btnFacebook = document.getElementById('btnfacebook');
    btnLogin.addEventListener('click', function () {
        /// TODO 2: Add email login button event
        ///         1. Get user input email and password to login
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" and clean input field
        firebase.auth().signInWithEmailAndPassword(txtEmail.value,txtPassword.value).then(function(){
            window.location.href="choose.html";
        })
        .catch(function(error){
                var errorCode=error.code;
                var errorMessage=error.message;  
                create_alert("error",errorMessage);      
        });
    });

    btnGoogle.addEventListener('click', function () {
        /// TODO 3: Add google login button event
        ///         1. Use popup function to login google
        ///         2. Back to index.html when login success
        ///         3. Show error message by "create_alert" 
        var provider=new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result){
            var token=result.credential.accessToken;
            var user=result.user;
            console.log("in");
            window.location.href="choose.html";
        }).catch(function(error){
            var errorCode=error.code;
            var errorMessage=error.message;
            var email=error.email;
            var credential =error.credential;
            create_alert("error",errorMessage);
        })
        
    });

    btnFacebook.addEventListener('click', function () {
        var provider=new firebase.auth.FacebookAuthProvider(); 
        console.log(provider);
        firebase.auth().signInWithPopup(provider).then(function(result){
            console.log("in");
            var token=result.credential.accessToken;
            var user=result.user;
            window.location.href="choose.html";
        }).catch(function(error){
            var errorCode=error.code;
            var errorMessage=error.message;
            var email=error.email;
            var credential =error.credential;
            create_alert("error",errorMessage);
        })
        
    });
    
    btnSignUp.addEventListener('click', function () {        
        /// TODO 4: Add signup button event
        ///         1. Get user input email and password to signup
        ///         2. Show success message by "create_alert" and clean input field
        ///         3. Show error message by "create_alert" and clean input field
        firebase.auth().createUserWithEmailAndPassword(txtEmail.value,txtPassword.value).then(function(){
            window.location.href="choose.html";
        }).catch(function(error){
            var errorCode=error.code;
            var errorMessage=error.message;
            create_alert("error",errorMessage);
        })
    });
}

// Custom alert
function create_alert(type, message) {
    var alertarea = document.getElementById('custom-alert');
    if (type == "success") {
        str_html = "<div class='alert alert-success alert-dismissible fade show' role='alert'><strong>Success! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    } else if (type == "error") {
        str_html = "<div class='alert alert-danger alert-dismissible fade show' role='alert'><strong>Error! </strong>" + message + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
        alertarea.innerHTML = str_html;
    }
}

window.onload = function () {
    initApp();
};

