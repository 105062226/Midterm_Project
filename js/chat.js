function init(){
    var user_email="";
    var scroll=document.getElementById("messages");

    
    window.addEventListener('unload',function(e){
        console.log("in");
    });
    function myFunction() {
        return "Write something clever here...";
    }
    function choose_img(pic){
        console.log(pic);
        switch(pic){
            case "Basket":
                return "<img src=img/1.jpg id=selfphoto>"
            case "Business":
                return "<img src=img/2.jpg id=selfphoto>"
            case "College":
                return "<img src=img/3.png id=selfphoto>"
            case "Wizard":
                return "<img src=img/4.jpg id=selfphoto>"
            case "Engineer":
                return "<img src=img/5.png id=selfphoto>"
            case "Wonder":
                return "<img src=img/6.jpg id=selfphoto>"
        }
    }

    firebase.auth().onAuthStateChanged(function(user){
        var who = document.getElementById("user-name");
        var num_of_people=0;
        var a=0;
        var b=0;
        var selfie="";
        if(user){
            var login =firebase.auth().currentUser;
            var photo = firebase.database().ref("num_of_user");
            user_email=user.email;
            //document.getElementById("user_title").innerHTML=user.email;
            photo.on('value',function(snap){
                snap.forEach(function(child){
                    if(user_email==child.child("who_is_online").val()){
                        selfie=choose_img(child.child("picture").val());
                        document.getElementById("user_title").innerHTML=selfie+" "+user.email;
                    }
                });
            });
            
            
            document.getElementById("sign-out").addEventListener("click",function(){
                firebase.database().ref("num_of_user/"+ login.uid).remove().then(function(){
                    console.log("success")
                });
            
                firebase.auth().signOut().then(function() {
                    window.location.href="index.html";
                }, function(error) {
                    console.error('Sign Out Error', error);
                });
            });

            
            var change=firebase.database().ref('num_of_user');
            var before_total_user=[];
            var total_user=[];
            var num=0;
            change.on('value',function(snapshot){
                snapshot.forEach(function(childSnapshot){   
                        total_user[num]="<li><a href=#>"+childSnapshot.child("who_is_online").val()+"</a></li>";  
                        num+=1;                        
                })
                console.log(total_user,before_total_user);

                document.getElementById('whoin').innerHTML=total_user.join('');
                num=0;
                total_user=[];
            });
        }

        else{
            who.innerHTML="<div id=user-name></div>";
            //document.getElementById("message-filler").innerHTML="";
        }

    });



    send_btn =document.getElementById("submit");
    send_messege=document.getElementById("message");
    console.log(send_messege.value);
    var picture="";
    send_btn.addEventListener("click",function(){
        if(send_messege.value!=""){
            var d=new Date();
            var sending_time=d.toLocaleTimeString();
            var user_picture=firebase.database().ref("num_of_user");
            user_picture.on('value',function(snap){
                snap.forEach(function(child){
                    if(user_email==child.child("who_is_online").val()){
                        picture=choose_img(child.child("picture").val()); 
                    }
                });
            });

            var newPostRef =firebase.database().ref("com_list").push();    
            newPostRef.set({
            message:send_messege.value,
            user:user_email,
            time:sending_time,
            user_image:picture
        })
            document.getElementById('message').value="";
        } 
    }); 
    

    window.addEventListener('keydown', function (e) {
        if(e.keyCode==13) {
            if(send_messege.value!=""){
                var d=new Date();
                var sending_time=d.toLocaleTimeString();
                var user_picture=firebase.database().ref("num_of_user");
                user_picture.on('value',function(snap){
                    snap.forEach(function(child){
                        if(user_email==child.child("who_is_online").val()){
                            picture=choose_img(child.child("picture").val()); 
                        }
                    });
                });
    
                var newPostRef =firebase.database().ref("com_list").push();    
                newPostRef.set({
                message:send_messege.value,
                user:user_email,
                time:sending_time,
                user_image:picture
            })
                document.getElementById('message').value="";
            } 
        }
        else {
            
        }
    });

    var str_before_username=""
    var str_after_content = "<br><br></div></div>\n";
    var postsRef = firebase.database().ref('com_list');
    var total_post = [];
    var first_count = 0;
    var second_count = 0;
    var which_image="";
    

    postsRef.once('value')
    .then(function (snapshot) {

        snapshot.forEach(function(childSnapshot){
            var childData=childSnapshot.val();
            childData.email=childSnapshot.child("user").val();
            if(childData.email==user_email)
            {
                document.getElementById('message-filler').innerHTML+=str_before_username+"<span id=user_say>"+childData.user_image+
                "</span>"+str_after_content+"<span id=user_send>"+childData.message+"</span>"+"<span id=user_time>"+childData.time+"</span>"+str_after_content;
            }
            else
            {
                document.getElementById('message-filler').innerHTML+=str_before_username+"<span id=other_say>"+childData.user_image+" "+childData.email+
                "</span>"+str_after_content+"<span id=other_send>"+childData.message+"</span>"+"<span id=other_time>"+childData.time+"</span>"+str_after_content;
            }
            first_count+=1;
        });


        postsRef.on('child_added',function(data){
            second_count+=1; 
            var who_send;
            var what_send;
            var when_send;
            var image;
            if(second_count>first_count){
                    postsRef.orderByKey().limitToLast(1).on('value',function(snapshot){
                    snapshot.forEach(function(data){
                    var childData=data.val();
                    who_send=data.val().user;
                    what_send=data.val().message;
                    when_send=data.val().time;
                    image=data.val().user_image;
                })
                });
                
                if(who_send==user_email){
                    document.getElementById('message-filler').innerHTML+=str_before_username+"<span id=user_say>"+image+"</span>"+
                    str_after_content+"<span id=user_send>"+what_send+"</span>"+"<span id=user_time>"+when_send+"</span>"+str_after_content;
                }
                else
                {
                    document.getElementById('message-filler').innerHTML+=str_before_username+"<span id=other_say>"+image+" "+
                    who_send+"</span>"+str_after_content+"<span id=other_send>"+what_send+"</span>" +"<span  id=other_time>"+when_send+"</span>"+str_after_content;
                }
                
                $('#' +"messages").animate({
                    scrollTop: scroll.scrollHeight - scroll.clientHeight
                 }, 500);
            } 
        });
        
    })

    .catch(e => console.log(e.message));
}


window.onload = function () {
    init();
};

Notification.requestPermission().then(function(result) {
    console.log(result);
    // Do something with the granted permission.
    if(result==="granted"){
        var notification = new Notification("Welcome to NTHU Chat");
    }
  });
